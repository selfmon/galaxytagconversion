# GalaxyTagConversion

Some very old code in PHP from a locked area on the SM-Alarms website before the new SelfMon site was brought online. I've decided to share this algorithm in order that low cost tags can be used on the Galaxy MAX readers.  

Note that for Honeywell tags, the client ID is always 01. If you use low cost import tags, you need to know the client ID. Some low cost scanners will scan and display this.

## I've included a call to the function in the code as an example


```
php tagConvert.php
Galaxy prox reader codes to MAX reader conversion
Array
(
    [0] => 1234567 : 4776629
    [1] => 543210 : 298339
    [2] => 121212 : 191834
)
MAX reader codes to prox reader conversion (This is the black tag code converted - the keyprox scans this code
Array
(
    [0] => 1234567 : 2193289
    [1] => 543210 : 138098
    [2] => 121212 : 294006
)
Low cost import tags with a client code converted for MAX reader
Array
(
    [0] => 1234567 : 893585543
    [1] => 543210 : 892894186
    [2] => 121212 : 892472188
)


```

