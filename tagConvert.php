<?php

/*
 * tagConvert.php
 *
 *  Created on: Jan 17, 2008
 *  Author: Stuart.
 *  Copyright: SM Alarms 2008
 *
 *  Algorithm implementation for converting KeyProx tag codes to external MAX tag codes.
 *
 *  Arg 1 decno   Decimal tag number with multiples comma separated.
 *  Arg 2 type    Conversion type mp for MAX to PROX  &  pm for PROX to MAX conversion.
 *  Arg 3 client  Tag client code - Honeywell tags are always coded 1.
 *
 *  Returns an array of original codes and the conversions.
 *
 */


function tagconvert($decno, $type, $client) {

	$decnos = explode(',',$decno);

	$inputcode="";

	if (sizeof($decnos)>100) {$inputcode="sorry no more than 100 codes";}
	elseif(sizeof($decnos)==0) {$inputcode="sorry, you didn't add a code";}

	foreach($decnos as $decno) {

		if(strlen($decno)<6) {

			$inputcode="sorry, tag code $decno  wasn't long enough";
			$retres[] = $decno." : ".$inputcode;

		} else {

			if ($client == 1) {
				$inputcode="";
				$bin = sprintf( "%024b", $decno );
				$res = sprintf( "%024b", 0 );
				$fin = sprintf( "%024b", 0 );

				if ($type == "mp") {

					$conversion = array_reverse(
							    array(7,8,10,4,2,1,11,9,6,0,3,5,13,
							    		15,12,14,18,16,19,17,21,23,20,22)
							);

				} else if ($type == "pm") {

					$conversion = array_reverse(
							    array(9,5,4,10,3,11,8,0,1,7,2,6,14,
							    		12,15,13,17,19,16,18,22,20,23,21)
							);

				} else {
					return(-1);
				}

				for ($i=23; $i>=0; $i--) {
					$res[$conversion[$i]] = $bin[$i];
				}

				$j=0;
				for ($i=23; $i>=0; $i--) {
					$fin[$j] = $res[$i];
					$j++;
				}
				$retres[] = $decno." : ".bindec($fin);
			} else {

				$finno = substr(($client * 4294967296) + $decno, -9);
				$retres[] = $decno." : ".$finno;
			}
		}
	}

	return($retres);
}

print("Galaxy prox reader codes to MAX reader conversion\n");
print_r(tagconvert("1234567,543210,121212", "pm", "01"));

print("MAX reader codes to prox reader conversion (This is the black tag code converted - the keyprox scans this code\n");
print_r(tagconvert("1234567,543210,121212", "mp", "01"));

print("Low cost import tags with a client code converted for MAX reader\n");
print_r(tagconvert("1234567,543210,121212", "pm", "81"));
?>

